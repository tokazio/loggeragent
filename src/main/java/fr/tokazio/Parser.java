package fr.tokazio;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;

import static fr.tokazio.TimingData.H;

public class Parser {

    private TimingData current = new TimingData();

    public static void main(String[] args) throws IOException {
        /*
        new Parser().parse("C:\\Users\\rpetit.GROUPE-WELCOOP\\Desktop\\testTimeLine.txt")
        .draw("C:\\Users\\rpetit.GROUPE-WELCOOP\\Desktop\\testTimeLine.png");
        */
        new Parser().parse("C:\\Users\\rpetit.GROUPE-WELCOOP\\Desktop\\a mettre en timeline.txt")
                //.draw("C:\\Users\\rpetit.GROUPE-WELCOOP\\Desktop\\a mettre en timeline.png");
                .web("C:\\\\Users\\\\rpetit.GROUPE-WELCOOP\\\\Desktop\\\\a mettre en timeline.html");
    }

    public Parser parseStr(String toString) {
        String[] strs = toString.split("\\r?\\n");
        for (String line : strs) {
            //System.out.println(line);
            if (line.startsWith("TIME START")) {
                current = current.down(line);
                //System.out.println("START Current is "+current.className());
            } else if (line.startsWith("TIME END")) {
                TimingData was = current;
                TimingData td = current.up(line);
                if (td != null) {
                    current = td;
                }
                //System.out.println("END Current is "+current.className()+" was "+was.className());
            }
        }
        return this;
    }

    public Parser parse(String filename) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line = reader.readLine();
            while (line != null) {
                //System.out.println(line);
                if (line.startsWith("TIME START")) {
                    current = current.down(line);
                    //System.out.println("START Current is "+current.className());
                } else if (line.startsWith("TIME END")) {
                    TimingData was = current;
                    TimingData td = current.up(line);
                    if (td != null) {
                        current = td;
                    }
                    //System.out.println("END Current is "+current.className()+" was "+was.className());
                }
                // read next line
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this;
    }

    public Parser web(String filename) throws IOException {
        Files.write(new File(filename).toPath(), web().getBytes());
        return this;
    }

    public String web() {
        //System.out.println("Level max: "+current.maxLevel(0));
        final StringBuilder sb = new StringBuilder();
        sb.append("<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\n" +
                "\n" +
                "<script type=\"text/javascript\">\n" +
                "  google.charts.load(\"current\", {packages:[\"timeline\"]});\n" +
                "  google.charts.setOnLoadCallback(drawChart);\n" +
                "  function drawChart() {\n" +
                "    var container = document.getElementById('example4.2');\n" +
                "    var chart = new google.visualization.Timeline(container);\n" +
                "    var dataTable = new google.visualization.DataTable();\n" +
                "\n" +
                "    dataTable.addColumn({ type: 'string', id: 'Role' });\n" +
                "    dataTable.addColumn({ type: 'string', id: 'Name' });\n" +
                "    dataTable.addColumn({ type: 'string', role: 'tooltip' });\n" +
                "    dataTable.addColumn({ type: 'date', id: 'Start' });\n" +
                "    dataTable.addColumn({ type: 'date', id: 'End' });\n" +
                "    dataTable.addRows([");
        current.visit3(sb, 0);
        sb.append("  \n" +
                "\t  \n" +
                "  ]);\n" +
                "\n" +
                "    var options = {\n" +
                "      timeline: { groupByRowLabel: true, showBarLabels: true, showRowLabels:false }\n" +
                "    };\n" +
                "\n" +
                "    chart.draw(dataTable, options);\n" +
                "  }\n" +
                "</script>\n" +
                "\n" +
                "<div id=\"example4.2\" style=\"width: " + (current.timeTotal() <= 0 ? current.timeTotal() + "px" : "100%") + ";height: " + (current.maxLevel(0) * 50) + "px;\"></div>");
        return sb.toString();
    }

    public Parser draw(String filename) throws IOException {
        System.out.println("Temps total: " + current.timeTotal());
        System.out.println("Level max: " + current.maxLevel(0));

        final int w = 4096;
        final int h = Math.max(1, current.maxLevel(0)) * H;

        float scale = 0.01f;

        System.out.println("Scale is " + scale);

        final BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = bi.createGraphics();
        g2d.setFont(g2d.getFont().deriveFont(H / 2f));
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, w, h);

        current.visit2(g2d, 0, current.timeStart(), w, scale);
        ImageIO.write(bi, "png", new File(filename));

        return this;
    }


}
