package fr.tokazio;

public class RenderEncaissement implements Render {

    @Override
    public String className() {
        return "fr.atiss.soft.cliserv.container.encaissement.Encaissement";
    }

    public String after(final String methodName) {
        if (methodName.contains("save")) {
            return "\"SAVE (" + methodName + ") encaissement #\" + $_.getId() + \" pour l'acte #\" + $_.getIdActe()+\" de \"+ $_.getMontant()+\" € en '\" + $_.getModeReglement()+\"'\"" + keyval("$_");
        }
        return "";
    }

    @Override
    public String before(String methodName) {
        if (methodName.contains("delete")) {
            return "\"DELETE (" + methodName + ") encaissement #\" + $1.getId() + \" pour l'acte #\" + $1.getIdActe()+\" de \"+ $1.getMontant()+\" € en '\" + $1.getModeReglement()+\"'\"" + keyval("$1");
        }
        return "";
    }

    private String keyval(String v) {
        return "+\"|{idActe=\"+" + v + ".getIdActe()+\",idEncaissement=\"+" + v + ".getId()+\"}\"";
    }
}
