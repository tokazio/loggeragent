package fr.tokazio;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public class TimingData {

    public static int H = 60;
    static int k = 0;
    private final TimingData parent;
    private final List<TimingData> children = new LinkedList<>();
    private final Color[] palette = new Color[]{

            Color.CYAN, Color.MAGENTA, Color.ORANGE, Color.RED

    };
    private long id = 0;
    private long timestart = 0;
    private long timeend = 0;
    private String className = "Base";

    public TimingData(TimingData timingData, String line) {
        this.parent = timingData;
        this.id = getIdFromLine(line);
        this.timestart = getTime(line);
        final int cl = line.indexOf('#') + 18;
        this.className = line.substring(cl);
    }

    public TimingData() {
        parent = null;
    }

    public static long getTime(String line) {
        final int start = line.indexOf('@');
        return Long.parseLong(line.substring(start + 1, start + 1 + 13));
    }

    public static long getIdFromLine(String line) {
        final int id = line.indexOf('#');
        return Long.parseLong(line.substring(id + 1, id + 1 + 16));
    }

    public void end(String line) {
        this.timeend = getTime(line);
    }

    public long getId() {
        return id;
    }

    public long timeStart() {
        if ("Base".equals(className)) {
            return children.get(0).timeStart();
        }
        return timestart;
    }

    public long duration() {
        return timeend > 0 && timeend > timestart ? timeend - timestart : -1;
    }

    public long timeTotal() {
        if (children.isEmpty()) {
            return duration();
        }
        long tot = 0;
        for (TimingData child : children) {
            tot += child.timeTotal();
        }
        return tot;
    }

    public int maxLevel(int nb) {
        if (children.isEmpty()) {
            return nb;
        }
        int max = 0;
        for (TimingData child : children) {
            int m = child.maxLevel(nb + 1);
            if (m > max) {
                max = m;
            }
        }
        return max;
    }

    public long timeEnd() {
        return timeend;
    }

    public String className() {
        return className;
    }

    @Override
    public String toString() {
        return "TimingData{" + "id=" + id +
                ", duration=" + duration() + "ms, timeStart=" + timestart +
                ", timeEnd=" + timeend +
                ", className='" + className + '\'' +
                '}';
    }

    public TimingData down(String line) {
        final TimingData child = new TimingData(this, line);
        children.add(child);
        return child;
    }

    public TimingData up(String line) {
        final long eid = getIdFromLine(line);
        if (eid == id) {
            end(line);
            return parent;
        }
        if (parent != null) {
            return parent.up(line);
        }
        return null;
    }

    public void visit(StringBuilder sb, int nb) {
        for (int i = 0; i < nb; i++) {
            sb.append("----");
        }
        sb.append(toString()).append("\n");
        for (TimingData child : children) {
            child.visit(sb, nb + 1);
        }
    }

    public void visit3(StringBuilder sb, int nb) {
        if (!"Base".equals(className) && timeend > 0) {
            sb.append("[ '").append(nb).append("',  '', '").append(className()).append("',   new Date(").append(timeStart()).append("),  new Date(").append(timeEnd()).append(") ],\n");
        }
        for (TimingData child : children) {
            child.visit3(sb, nb + 1);
        }
    }

    public void visit2(Graphics2D g2d, int i, long timestart, long timewindow, float scale) {
        k++;
        int x = (int) ((timeStart() - timestart) * scale);
        int h = H;
        int w = (int) (duration() * scale);
        if (w <= 0) {
            w = 10;
        }
        int y = (i - 1) * H;
        g2d.setColor(palette[k % 3]);
        g2d.fillRect(x, y, w, h);
        g2d.setColor(Color.BLACK);
        g2d.drawRect(x, y, w, h);
        int ty = y + g2d.getFontMetrics().getHeight();
        g2d.drawString(className(), x + 10, y + ty);
        System.out.println("#" + i + ": " + x + "," + y + " " + w + "," + h + " " + className());
        for (TimingData child : children) {
            child.visit2(g2d, i + 1, timestart, timewindow, scale);
        }
    }

}
