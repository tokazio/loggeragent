[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://bitbucket.org/tokazio/loggeragent)

# LoggerAgent

## use as:

java -jar -javaagent:LoggerAgent-1.0-SNAPSHOT-all.jar=./traces.yaml;no-http lejaralogger.jar

The agent change the System.out to handle future http requests.


# or via jnlp

//todo

# or launch.sh

/home/pharmagest/bin/launch.sh

add:

```
-javaagent:$HOME/loggeragent-1.1-SNAPSHOT-all.jar=$HOME/traces.txt \
```

to the java vm args at the bottom, like:
```
set -x
{
  $JREPATH_CLIENT/bin/java \
    -javaagent:$HOME/loggeragent-1.1-SNAPSHOT-all.jar=$HOME/traces.txt \
    -showversion \
    -Xmx512m \
    -XX:MaxPermSize=256m \
    -DDISPLAY=$DISPLAY \
    -Dserver=$H \
    -DSOURCE=$DESIGNATION \
    -DDOMSANTE=http://www.pharmattitude.fr \
    -DREPORT10G=$REPORT10GVALUE \
    -Djava.protocol.handler.pkgs=fr.atiss.soft.protocols \
    -Dlog4j.configuration=conf/$JFICLOG \
        -Dspring.profiles.active=PRODUCTION \
        -Dspring.context.location=fr.atiss.soft.configuration.AppCfgLgpi \
    -Djava.library.path=$TMPDIR \
    -Dsun.awt.fontconfig=$PHARMA_CONF/fontconfig.lgpi.7.properties \
    -Dbrowser.force.javafx=$JAVAFX \
    -Djxbrowser.forceLightweight=true \
    $ARGS \
    fr.atiss.soft.portail.Lgpi
} > $LOG 2>&1
```


//todo example

## Show log via http

http://localhost:8311/agent

## Show timeline via htpp

http://localhost:8311/log

//todo image

## Viewer

java -jar LoggerAgent-1.0-SNAPSHOT-all.jar <resultLogFileNameToParse>

### Modes

* after
* before
* time

//tod count call number