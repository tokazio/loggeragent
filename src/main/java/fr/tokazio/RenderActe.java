package fr.tokazio;


public class RenderActe implements Render {

    @Override
    public String className() {
        return "fr.atiss.soft.cliserv.container.encaissement.Acte";
    }

    public String after(final String methodName) {
        if (methodName.contains("save")) {
            return "\"SAVE (" + methodName + ") acte #\" + $_.getId() + \" du type '\" + $_.getTypeActe().getLibelle()+ \"' avec \" + $_.getFactures().size() + \" facture(s)\"" + keyval();
        }
        return "";
    }

    @Override
    public String before(String methodName) {
        return null;
    }

    private String keyval() {
        return "+\"|{idActe=\"+$_.getId()+\"}\"";
    }
}
