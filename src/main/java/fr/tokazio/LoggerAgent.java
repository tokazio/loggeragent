package fr.tokazio;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import javassist.*;
import org.slf4j.instrumentation.JavassistHelper;

import java.io.*;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.ProtectionDomain;
import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

public class LoggerAgent implements ClassFileTransformer {

    public static final String[] DEFAULT_EXCLUDES = new String[]{
            "com/sun/",
            "sun/",
            "java/",
            "javax/",
            "org/slf4j",
            "org/apache",
            "org/xml",
            "fr/tokazio/Config",
            "oracle",
            "com/teamdev",
            "javafx",
            "com/fasterxml",
            "fr/out"
    };

    //logger field to add to classes
    //private static final String loggerFieldName = "_loggerAgentlog";
    //private static final String JUL_DEF = "private static java.util.logging.Logger " + loggerFieldName + ";";

    private static final String LOG_FILE = "/var/log/lgpi/logger-agent.log";

    private static Config config;
    private static boolean withHttp = true;

    final ClassPool pool = ClassPool.getDefault();
    private final ServiceLoader<Render> loader = ServiceLoader.load(Render.class);

    static final Parser parser = new Parser();
    private static final java.io.ByteArrayOutputStream out = new java.io.ByteArrayOutputStream();

    public static void premain(String agentArgs, Instrumentation inst) throws IOException, URISyntaxException {
        System.out.println("LoggerAgent started with " + agentArgs);
        System.out.println("LoggerAgent from " + thisJar());
        inst.addTransformer(new LoggerAgent(), inst.isRetransformClassesSupported());
        if (agentArgs != null && !agentArgs.isEmpty()) {
            boolean multi = agentArgs.indexOf(',') > 0;
            if (!multi) {
                config = Config.load(agentArgs);
            } else {
                String[] args = agentArgs.split(",");
                for (String arg : args) {
                    if ("no-http".equals(arg)) {
                        withHttp = false;
                    } else {
                        config = Config.load(args[0]);
                    }
                }
            }
        }

        /*
        try {
            CtClass cl = ClassPool.getDefault().makeClass(new ByteArrayInputStream(fromJar()));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        */

        if (withHttp) {
            System.setOut(new java.io.PrintStream(out));
            HttpServer server = HttpServer.create(new InetSocketAddress(8311), 0);
            server.createContext("/agent", new MyHandler());
            server.createContext("/log", new MyLogHandler());
            server.setExecutor(null); // creates a default executor
            System.out.println("LoggerAgent HTTP server started on 8311: http://localhost:8311/agent");
            server.start();
        }
    }

    static File thisJar() throws URISyntaxException, MalformedURLException {
        return new File(LoggerAgent.class.getProtectionDomain().getCodeSource().getLocation().toURI());
    }

    static byte[] fromJar() throws IOException, URISyntaxException {
        System.out.println("openning jar " + thisJar());
        JarFile jarfile = new JarFile(thisJar());
        System.out.println("getting entry fr/tokazio/StackTracer.class");
        ZipEntry jarEntry = jarfile.getEntry("fr/tokazio/StackTracer.class");
        System.out.println("reading entry...");
        byte[] buffer = new byte[(int) jarEntry.getSize()];
        try (InputStream in = new BufferedInputStream(jarfile.getInputStream(jarEntry))) {
            for (; ; ) {
                int nBytes = in.read(buffer);
                if (nBytes <= 0) break;
            }
        }
        System.out.println("got " + buffer.length + " bytes, ok");
        return buffer;
    }

    private byte[] doClass(final String name, final Class clazz, byte[] b) {
        CtClass cl = null;
        try {
            cl = pool.makeClass(new java.io.ByteArrayInputStream(b));
            if (!cl.isInterface()) {
                //final CtField field = CtField.make(JUL_DEF, cl);
                //final String getJULLogger = "java.util.logging.Logger.getLogger(" + name.replace('/', '.') + ".class.getName());";

                CtMethod mHostname = CtNewMethod.make(
                        "private String _loggeragenthostname(){\n" +
                                "        try {\n" +
                                "            return java.net.InetAddress.getLocalHost().getHostName();\n" +
                                "        } catch (Exception e) {\n" +
                                "            return \"LoggerAgent Erreur de récupération du nom de poste\";\n" +
                                "        }\n" +
                                "    }", cl);
                cl.addMethod(mHostname);

                CtMethod m = CtNewMethod.make(
                        "private void _loggerAgentLog(String val) {\n" +
                                "        if(val==null || val.isEmpty()) return;\n" +
                                "        val = \"TRACE|\"+ new java.text.SimpleDateFormat(\"yyyy-MM-dd'T'HH:mm:ss'Z'\", java.util.Locale.US).format(new java.util.Date())+\"|\"+_loggeragenthostname()+\"|\"+val;\n" +
                                "        System.out.println(val);\n" +
                                "        java.io.PrintWriter out = null;\n" +
                                "        try {\n" +
                                "            out = new java.io.PrintWriter(new java.io.BufferedWriter(new java.io.FileWriter(\"" + LOG_FILE + "\", true)));\n" +
                                "            out.println(val);\n" +
                                "        } catch (java.io.IOException e) {\n" +
                                "            e.printStackTrace();\n" +
                                "        } finally {\n" +
                                "            if(out!=null) {\n" +
                                "                out.close();\n" +
                                "            }\n" +
                                "        }\n" +
                                "    }",
                        cl);
                cl.addMethod(m);

                /*
                CtMethod m2 = CtNewMethod.make(
                        "private void _loggerAgentCallStack() {\n" +
                             //   "Object[] t = Thread.currentThread().getStackTrace();\n"+
                             //   "for (int i = 1; i < t.length; i++) {\n"+
                             //          "System.out.print(\"TIME >\");\n"+
                             //          "System.out.println(t[i]);\n"+
                             //   "}"+
                        "}",
                        cl);
                cl.addMethod(m2);
                */

                //cl.addField(field, getJULLogger);
                byte[] byteCode = null;
                for (CtBehavior method : cl.getDeclaredBehaviors()) {
                    //todo CtConstructor ?
                    if (method instanceof CtMethod && !method.isEmpty()) {
                        if (doMethod(name, (CtMethod) method, cl)) {
                            byteCode = cl.toBytecode();
                            cl.defrost();
                            /*save generated bytecode to file for debugging*/
                            new File("C:\\var\\log\\lgpi\\loggeragent\\" + name.substring(0, name.lastIndexOf("/"))).mkdirs();
                            try (FileOutputStream fos = new FileOutputStream("C:\\var\\log\\lgpi\\loggeragent\\" + name + ".class")) {
                                fos.write(byteCode);
                            }
                        }
                    }
                }
                b = byteCode != null ? byteCode : cl.toBytecode();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("LoggerAgent could not instrument  " + name + ",  exception : " + e.getClass().getName() + " " + e.getMessage());
        } finally {
            if (cl != null) {
                cl.detach();
            }
        }
        return b;
    }

    private void instru(final Rule rule, final String className, final CtMethod method, final CtClass cl) {
        try {
            //final String signature = JavassistHelper.getSignature(method);
            //final String returnValue = JavassistHelper.returnValue(method);
            final String returnType = method.getReturnType().getName();
            System.out.println("| \tLoggerAgent processing mode '" + rule.mode() + "'...");


            if (rule.mode() == null || rule.mode().isEmpty() || rule.mode().equals("before")) {

                String p1 = "";
                if (method.getParameterTypes().length > 0) {
                    p1 = method.getParameterTypes()[0].getName();
                }

                String r = before(p1, rule);
                if (r == null) {
                    if (!rule.trace().isEmpty()) {
                        method.insertBefore("_loggerAgentLog(\"" + rule.trace() + "\");");
                        System.out.println("| \tLoggerAgent added [before] from rule trace (" + rule.trace() + ")");
                    } else {
                        System.out.println("| \tLoggerAgent no renderer for '" + returnType + "' nor rule trace");
                    }
                } else {
                    method.insertBefore("_loggerAgentLog(" + r + ");");
                    System.out.println("| \tLoggerAgent added [before] from a renderer (" + r + ")");
                }
            }
            if (rule.mode() == null || rule.mode().isEmpty() || rule.mode().equals("after")) {
                if (!returnType.equals("void")) {
                    String r = after(returnType, rule);
                    if (r == null) {
                        if (!rule.trace().isEmpty()) {
                            method.insertAfter("_loggerAgentLog(\"" + rule.trace() + "\");");
                            System.out.println("| \tLoggerAgent added [after] from rule trace (" + rule.trace() + ")");
                        } else {
                            System.out.println("| \tLoggerAgent no renderer for '" + returnType + "' nor rule trace");
                        }
                    } else {
                        method.insertAfter("_loggerAgentLog(" + r + ");");
                        System.out.println("| \tLoggerAgent added [after] from a renderer (" + r + ")");
                    }
                } else {
                    System.out.println("| \tLoggerAgent [after] on void method not handled (" + rule.trace() + ")");
                }
            }


            if ("time".equals(rule.mode())) {
             /*   if(Modifier.isSynchronized(method.getModifiers())){
                    System.out.println("This 'synchronized' method can't be instrumented: "+method.getSignature());
                }
                if(Modifier.isStatic(method.getModifiers())){
                    System.out.println("This 'static' method can't be instrumented: "+method.getSignature());
                }

              */
                method.addLocalVariable("_loggerAgentTimeStart", CtClass.longType);
                method.addLocalVariable("_loggerAgentTimeUuid", CtClass.longType);

                // method.insertBefore("new Exception(Long.toString(_loggerAgentTimeUuid)).printStackTrace();");
                method.insertBefore("fr.tokazio.StackTracer.stackTrace();");
                method.insertBefore("System.out.println(\" " + method.getLongName() + "\");");
                method.insertBefore("System.out.print(\" \"+Thread.currentThread().getName());");
                method.insertBefore("System.out.print(_loggerAgentTimeUuid);");
                method.insertBefore("System.out.print(\"@\"+System.currentTimeMillis()+\" #\");");
                method.insertBefore("System.out.print(\"TIME START \");");
                method.insertBefore("_loggerAgentTimeUuid = Long.parseLong(String.valueOf(System.nanoTime()).concat(String.valueOf(Math.round(Math.random()))));");
                method.insertBefore("_loggerAgentTimeStart = System.currentTimeMillis();");
                //--
                method.insertAfter("System.out.print(\"TIME END \");");
                method.insertAfter("System.out.print(\"@\"+System.currentTimeMillis()+\" #\");");
                method.insertAfter("System.out.print(_loggerAgentTimeUuid);");
                method.insertAfter("System.out.println(\" " + method.getLongName() + " IN \"+(System.currentTimeMillis() - _loggerAgentTimeStart)+\"ms\");");
                //--
                System.out.println("| \tLoggerAgent added [time] to method " + rule.methodName());
            }

        } catch (NotFoundException | CannotCompileException ex) {
            System.out.println("| \tLoggerAgent nothing added because of this error:");
            ex.printStackTrace();
        }
    }


    @Override
    public byte[] transform(ClassLoader loader,
                            String className,
                            Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain,
                            byte[] origBytes) {


        for (int i = 0; i < DEFAULT_EXCLUDES.length; i++) {
            if (className.startsWith(DEFAULT_EXCLUDES[i])) {
                //System.out.println("LoggerAgent ignored " + className);
                return origBytes;
            }
        }

        /* filter !* from config
        final String n = className.replace("/",".");
        for(Rule rule : config.getRules()){
            if(rule.not()){
                if(n.startsWith(rule.methodName)) {
                    return origBytes;
                }
            }
        }
        */

        //System.out.println("LoggerAgent redefining " + className+"...");

        return doClass(className, classBeingRedefined, origBytes);
    }

    static class MyHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "";
            if (!parser.getCurrent().hasChildren()) {
                response = "No data";
            } else {
                response = parser.parseStr(out.toString()).web();
            }
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    /**
     * modify code and add log statements before the original method is called
     * and after the original method was called
     */
    private boolean doMethod(final String className, final CtMethod method, final CtClass cl) {
        if (config != null) {
            loop:
            for (Rule rule : config.getRules()) {
                if (rule.not()) {
                    continue loop;
                }
                final String signature;
                final String returnValue;
                final CtClass bcReturnType;
                try {
                    signature = JavassistHelper.getSignature(method);//CE TRUC MARCHE PAS
                    //returnValue = JavassistHelper.returnValue(method);
                    bcReturnType = method.getReturnType();
                } catch (NotFoundException e) {
                    System.err.println("LoggerAgent could not instrument  " + className + " " + method.getName() + ",  exception : " + e.getMessage());
                    continue loop;
                }
                final String name = method.getLongName();// className.replace("/", ".") + "." + signature;
                //System.out.println("-> " + name);

                //Modifier
                boolean next = false;
                switch (rule.modifier()) {
                    case "public":
                        next = Modifier.isPublic(method.getModifiers());
                        break;
                    case "private":
                        next = Modifier.isPrivate(method.getModifiers());
                        break;
                    case "protected":
                        next = Modifier.isProtected(method.getModifiers());
                        break;
                    case "package":
                        next = Modifier.isPackage(method.getModifiers());
                        break;
                    case "*":
                        next = true;
                        break;
                }
                if (!next) {
                    //System.out.println("\tDiscarded from modifier: " + rule.modifier() + "\n");
                    continue loop;
                }

                //Return type
                if (!rule.returnType().equals("*") && !rule.returnType().equals(bcReturnType.getName())) {
                    //System.out.println("\tDiscarded from returnType: " + bcReturnType.getName() + " instead of " + rule.returnType() + "\n");
                    continue loop;
                }

                //Method name
                if (rule.methodName().indexOf('*') >= 0 && !name.startsWith(rule.methodName().substring(0, rule.methodName().indexOf('*')))) {
                    //System.out.println("\tDiscarded from methodname '" + name + "' not starting with '" + rule.methodName().substring(0, rule.methodName().indexOf('*')) + "'\n");
                    continue loop;
                }
                System.out.println(">>Selected from methodname '" + name + "'");

                //Parameters
                if (rule.args() != null && rule.args().length > 0) {
                    for (int i = 0; i < rule.args().length; i++) {
                        //System.out.println("\t\t search " + args[i]);
                        if (!rule.args()[i].equals("*")) {
                            try {
                                if (method.getParameterTypes().length != rule.args().length) {
                                    //System.out.println("\tDiscarded from parameter type: no parameters \n");
                                    continue loop;
                                }
                                for (CtClass clazz : method.getParameterTypes()) {
                                    //System.out.println("\t\t org is " + clazz.getName());
                                    if (!rule.args()[i].equals(clazz.getName())) {
                                        //System.out.println("\tDiscarded from parameter type #" + i + ": " + rule.args()[i] + "\n");
                                        continue loop;
                                    }
                                }
                            } catch (NotFoundException e) {
                                System.err.println("LoggerAgent exception while searching parameters : " + e.getMessage());
                            }
                        }
                    }
                }
                //All filters are ok, intrumenting...
                System.out.println("| LoggerAgent processing '" + name + "'...");
                instru(rule, className, method, cl);
                System.out.println("| LoggerAgent ...processed '" + name + "'\n");
                return true;
            }
        }
        return false;
    }

    static class MyLogHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = out.toString();
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    private String after(String returnType, Rule rule) {
        final Iterator<Render> it = loader.iterator();
        while (it.hasNext()) {
            final Render p = it.next();
            if (p.className() != null && returnType.equals(p.className())) {
                return p.after(rule.methodName());
            }
        }
        return null;
    }

    private String before(String typeName, Rule rule) {
        final Iterator<Render> it = loader.iterator();
        while (it.hasNext()) {
            final Render p = it.next();
            if (p.className() != null && typeName.equals(p.className())) {
                return p.before(rule.methodName());
            }
        }
        return null;
    }



}