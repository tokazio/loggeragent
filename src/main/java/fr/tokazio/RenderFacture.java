package fr.tokazio;


public class RenderFacture implements Render {

    @Override
    public String className() {
        return "fr.atiss.soft.cliserv.container.facturation.Facture";
    }

    public String after(final String methodName) {
        if (methodName.contains("save")) {
            return "\"SAVE (" + methodName + ") facture #\" + $_.getId()\n" +
                    "                    + ($_.isFactureRemise() ? \" de remise en \" + $_.getTypeRemise() : \"\")\n" +
                    "                    + \" pour l'acte #\" + $_.getIdActe()" + keyval("$_");
        }
        return "";
    }

    @Override
    public String before(String methodName) {
        if (methodName.contains("delete")) {
            return "\"DELETE (" + methodName + ") facture #\" + $1.getId()\n" +
                    "                    + ($1.isFactureRemise() ? \" de remise en \" + $1.getTypeRemise() : \"\")\n" +
                    "                    + \" pour l'acte #\" + $1.getIdActe()" + keyval("$1");
        }
        if (methodName.contains("modifier")) {
            return "\"EDIT (" + methodName + ") facture #\" + $1.getId()\n" +
                    "                    + ($1.isFactureRemise() ? \" de remise en \" + $1.getTypeRemise() : \"\")\n" +
                    "                    + \" pour l'acte #\" + $1.getIdActe()" + keyval("$1");
        }
        return "";
    }

    private String keyval(String v) {
        return "+\"|{idActe=\"+" + v + ".getIdActe()+\",idFacture=\"+" + v + ".getId()+\"}\"";
    }


}
