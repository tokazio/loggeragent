package fr.tokazio;

public interface Render {

    String className();

    String after(final String methodName);

    String before(final String methodName);

}
