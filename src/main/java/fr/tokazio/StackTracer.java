package fr.tokazio;

import static fr.tokazio.LoggerAgent.DEFAULT_EXCLUDES;

public class StackTracer {

    /* Les 2 premiers
    java.lang.Thread::getStackTrace
    fr.tokazio.StackTracer::stackTrace
     */
    public static void stackTrace() {
        stack:
        for (int i = 2; i < Thread.currentThread().getStackTrace().length; i++) {
            final StackTraceElement el = Thread.currentThread().getStackTrace()[i];
            for (int j = 0; j < DEFAULT_EXCLUDES.length; j++) {
                if (el.getClassName().startsWith(DEFAULT_EXCLUDES[j].replaceAll("//", "."))) {
                    continue stack;
                }
            }
            System.out.println("TIME > " + el.getClassName() + "::" + el.getMethodName());
        }
    }
}
