package fr.tokazio;

import java.util.Arrays;

public class Rule {

    String modifier = "*";
    String returnType = "void";
    String methodName = "*";
    String[] args;
    String mode = "";//before / after / time
    boolean not = false;
    String trace = "";
    private String rule;

    public Rule(String str) {
        this.rule = str;
        parseRule(str);
    }

    public String rule() {
        return rule;
    }

    public String modifier() {
        return modifier;
    }

    public String returnType() {
        return returnType;
    }

    public String methodName() {
        return methodName == null ? "" : methodName;
    }

    public String[] args() {
        return args;
    }

    private void parseRule(String rule) {

        if (rule.startsWith("!")) {
            not = true;
            methodName = rule.substring(1);
            if (methodName.indexOf('*') >= 0) {
                methodName = rule.substring(0, methodName.indexOf('*')).replace("*", "");
            }
            return;
        }

        if (rule.indexOf('|') >= 0) {
            final String[] cmds = rule.split("\\|");
            rule = cmds[1];
            mode = cmds[0];
            if (cmds.length > 2) {
                trace = cmds[2];
            }
        }

        final String[] splits = rule.split("\\s");
        if (splits.length < 3 || splits[2].indexOf('(') < 0 || splits[2].indexOf(')') < 0) {
            //System.out.println("\t! " + s + " non valide, devrait être du type '*[modifier] *[returnType] *[methodName](*[parameterType][,...])'");
            throw new IllegalArgumentException(rule + " non valide, devrait être du type '*[modifier] *[returnType] *[methodName](*[parameterType][,...])'");
        }
        modifier = splits[0];
        returnType = splits[1];
        methodName = splits[2].substring(0, splits[2].lastIndexOf('('));
        final String argstr = splits[2].substring(splits[2].lastIndexOf('(')).replace("(", "").replace(")", "");
        args = argstr.split(",");

        //System.out.println("\treturnType=" + returnType + ", methodName=" + methodName + " args=" + Arrays.toString(args));
    }

    public String mode() {
        return mode;
    }

    public boolean not() {
        return not;
    }

    public String trace() {
        return trace;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Rule{");
        sb.append("rule='").append(rule).append('\'');
        sb.append(", modifier='").append(modifier).append('\'');
        sb.append(", returnType='").append(returnType).append('\'');
        sb.append(", methodName='").append(methodName).append('\'');
        sb.append(", args=").append(Arrays.toString(args));
        sb.append(", mode='").append(mode).append('\'');
        sb.append(", not=").append(not);
        sb.append(", trace='").append(trace).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
