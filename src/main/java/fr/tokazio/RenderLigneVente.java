package fr.tokazio;


public class RenderLigneVente implements Render {

    @Override
    public String className() {
        return "fr.atiss.soft.cliserv.container.facturation.LigneVente";
    }

    public String after(final String methodName) {
        if (methodName.contains("save")) {
            return "\"SAVE (" + methodName + ") ligne de vente #\" + $_.getId() + \" de la facture #\" + $_.getIdFacture()" + keyval("$_");
        }
        return "";
    }

    @Override
    public String before(String methodName) {
        if (methodName.contains("detruire")) {
            return "\"DELETE (" + methodName + ") ligne de vente #\" + $1.getId() + \" de la facture #\" + $1.getIdFacture()" + keyval("$1");
        }
        if (methodName.contains("modifier")) {
            return "\"EDIT (" + methodName + ") ligne de vente #\" + $1.getId() + \" de la facture #\" + $1.getIdFacture()" + keyval("$1");
        }
        return "";
    }

    private String keyval(String v) {
        return "+\"|{idActe=?,idFacture=\"+" + v + ".getIdFacture()+\",idLigneVente=\"+" + v + ".getId()+\"}\"";
    }

}
