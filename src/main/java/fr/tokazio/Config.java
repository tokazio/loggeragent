package fr.tokazio;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Config {

    private final List<Rule> rules = new ArrayList<>();

    public static Config load(String filename) {
        final File f = new File(filename);
        if (!f.exists()) {
            System.out.println("Configuration file '" + f + "' doesn't exists");
            System.exit(2);
        }
        final Config config = new Config();
        try {
            for (String str : Files.readAllLines(f.toPath(), StandardCharsets.UTF_8)) {
                if (!str.startsWith("//")) {
                    config.add(new Rule(str));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Configuration loaded:");
        for (Rule rule : config.rules) {
            System.out.println("\t> " + rule.rule());
        }
        return config;
    }

    public List<Rule> getRules() {
        return rules;
    }

    public void add(Rule rule) {
        rules.add(rule);
    }

    @Override
    public String toString() {
        return "Config{" +
                "rules=" + rules +
                '}';
    }
}
